import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../interfaces/user';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  constructor(public authservice:AuthService,
              public router:Router) { }

        email:string;
        password:string;
        user: Observable<User | null>

  onSubmit(){
    this.authservice.signUp(this.email,this.password);
  }
  ngOnInit() {
  }

}
