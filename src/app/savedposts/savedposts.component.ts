import { PostsService } from './../posts.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-savedposts',
  templateUrl: './savedposts.component.html',
  styleUrls: ['./savedposts.component.css']
})
export class SavedpostsComponent implements OnInit {

  constructor(private postsservice:PostsService,
    public authservice:AuthService) { }
    userId:string
    posts$:Observable<any>

  ngOnInit() {
    this.authservice.user.subscribe(
      user => {
        this.userId = user.uid;
        this.posts$ = this.postsservice.getSavedPosts(this.userId);
      }
    )
 
  }

}
