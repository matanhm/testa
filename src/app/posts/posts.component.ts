import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { PostsService } from '../posts.service';
import { Observable } from 'rxjs';
import { Comments } from '../interfaces/comments';
import { Posts } from '../interfaces/posts';
import { forkJoin } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { stringify } from 'querystring';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  comments$: Comments[];
  posts$: Posts[];
  title:String;
  body:String;
  author:String;
  success:boolean =false;
  pushAlert:string;
  userId:string;
  id: any;
  saved:string;
 // posts$:Observable<any>;
  constructor(private postsservice:PostsService,
              public authservice:AuthService,
              private route:ActivatedRoute) { }

  ngOnInit() {

    forkJoin(this.postsservice.getPosts(),this.postsservice.getComments()).subscribe(
           ([data1,data2])=>
           {
             this.posts$=data1,
             this.comments$=data2
           }
         )
         this.id = this.route.snapshot.params.id;
         this.authservice.user.subscribe(
           user=> {
             this.userId = user.uid;
           }
         )
         this.saved="Saved for later"
           
  }

  onSubmit(body:String){
    this.postsservice.savePost(this.userId,body)

  }
   // deletePost(id:string){
   //   this.postservice.deletePost(id,this.userId);
  //  }
  
  
 /* onSubmit(){
    for(let i=0; i<this.posts$.length;i++){
      for(let j=0; j<this.users$.length;j++){
        if(this.posts$[i].userId==this.users$[j].id){
          this.title = this.posts$[i].title;
          this.body = this.posts$[i].body;
          this.author= this.users$[j].name;
          this.postservice.savePosts(this.title,this.body,this.author)
        }
        this.success=true;
        this.pushAlert= "posts added";
      }
    }
  }*/


}
