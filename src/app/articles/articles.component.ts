import { Component, OnInit } from '@angular/core';
import { ArticlesService } from '../articles.service';

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.css']
})
export class ArticlesComponent implements OnInit {

  constructor(private articlesservice:ArticlesService) { }
  articles: any;
  ngOnInit() {

    this.articles=this.articlesservice.getArticles();
  }

}
