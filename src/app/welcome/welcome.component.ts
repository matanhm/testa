import { User } from './../interfaces/user';
import { AngularFireAuth } from '@angular/fire/auth';
import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  constructor(public authservice:AuthService,
              public angularfireAuth:AngularFireAuth) { }
  user: Observable<User | null>
  useremail:string

  ngOnInit() {
    this.authservice.user.subscribe(
      user=>{
        this.useremail = user.email
    }  
    )
    console.log(this.useremail)}
    

    
}
