import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { User } from './interfaces/user';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: Observable<User | null>
  err: any;

  constructor(public afAuth:AngularFireAuth,
              public router:Router) {
    this.user = this.afAuth.authState
   }

   signUp(email:string, password:string){
     this.afAuth
     .auth
     .createUserWithEmailAndPassword(email,password)
     .then(user => {
      this.router.navigate (['/welcome'])
       }).catch (
      error => this.err = error
       ) 
    }

   logOut(){
     this.afAuth.auth.signOut().then(res => console.log('Yahuu',res));
     this.router.navigate(['welcome'])
   }

   logIn(email:string, password:string){
    this.afAuth.auth.signInWithEmailAndPassword(email,password)
    .then(user=>{this.router.navigate(['/welcome']);
     }).catch (
    error => this.err = error
     ) 
    }
   
   
  }
