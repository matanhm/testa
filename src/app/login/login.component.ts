import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { User } from '../interfaces/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor( public authservice:AuthService,
                public router:Router,
                public route:ActivatedRoute) { }

        email:string;
        password:string;
        user: Observable<User | null>
  ngOnInit() {
  }

  onSubmit(){
    this.authservice.logIn(this.email,this.password);
  }

}
